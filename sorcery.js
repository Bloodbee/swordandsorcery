$( function() {
	//-----------------------
	// VARIABLES
	//-----------------------
	var buttons = $(".section button");	
	var type = "";
	
	//-----------------------
	// DEROULEMENT DU JEU
	//-----------------------
	startGame();
	
	//-----------------------
	// FONCTIONS
	//-----------------------
	/**
	* Fonction qui va afficher une div .status en fonction de la key. Utile pour avoir 2 sous-scénario lors d'une section
	*/	
	function displayStatus(key) {
		if(getTemperature() <= 100 && getTemperature() >= 0) // on display le contenu chaud
		{
			$("div#"+key+" .status span.cold").css("display", "none");
			$("div#"+key+" .status span.sex").css("display", "block");
			$("div#"+key+" .status span.hot").css("display", "block");
			actionGame($("div#"+key+":has(span.sex) action").attr('name'));
		}
		else if(getTemperature() <= 200 && getTemperature() > 100)
		{
			$("div#"+key+" .status span.cold").css("display", "none");
			$("div#"+key+" .status span.hot").css("display", "block");
			$("div#"+key+" .status span.sex").css("display", "none");
			actionGame($("div#"+key+":has(span.hot) action").attr('name'));
		}
		else if(getTemperature() > 200)
		{
			$("div#"+key+" .status span.cold").css("display", "block");
			$("div#"+key+" .status span.hot").css("display", "none");
			$("div#"+key+" .status span.sex").css("display", "none");
			actionGame($("div#"+key+":has(span.cold) action").attr('name'));
		}
		else
		{
			$("div#"+key+" .status span.cold").css("display", "none");
			$("div#"+key+" .status span.hot").css("display", "none");
			$("div#"+key+" .status span.sex").css("display", "none");
		}
	}
	
	/**
	* A chaque click de bouton
	*/	
	buttons.click( function() {
		//on récupère la valeur de l'attribut go, on appel gotoSection
		gotoSection($(this).attr("go"));
	} );
	
	/**
	* Fonction qui va changer la valeur de la div renseignant la temperature ambiante de la piece
	*/	
	function progress(type) {
		var largeur = $("#cache").width();
		
		if(type == "hot" && $("#cache").width() >= 20){
			$("#cache").width((largeur-20));
		}
		else if(type == "cold" && $("#cache").width() <= 340) {
			$("#cache").width((largeur+60));
		}
	}
	
	/**
	* Fonction servant au changement d'image du background.
	*/	
	function changeBackground(key) {
		$("#fond").css("background-image", "url('"+key+".png')");
	}
	
	/**
	* Fonction servant à se déplacer entre les différentes sections du jeu
	*/	
	function gotoSection(key) {
		//masque toute les sections et n'affiche que celle correspondant a key
		$(".section").hide();
		$("div#"+key).show();
		
		displayStatus(key);
		
		changeBackground(key);
		
		actionGame($("div#"+key+":has(span):visible action").attr('name'));
		
		actionGame($("div#"+key+":not(:has(span)) action").attr('name'));
	}
	
	/**
	* Fonction appelé lorsque le jeu commence
	*/	
	function startGame() {
		$(".section").hide();
		$("#intro").show();
		
		//execute l'action start
		actionGame("start");	
	}
	
	/**
	* Fonction appelé lorsque le jeu est fini
	*/	
	function endGame() {
		$(".section").hide();
		$("#intro").show();
		
		//execute l'action reset
		actionGame("reset");
	}
	
	/**
	* Fonction permettant de mettre en oeuvre une 'action'
	*/	
	function actionGame(act) {
		if(act == "start" || act == "reset") //commence le jeu
			$("#cache").width(280);
		else if(act == "hot")
			progress('hot');
		else if(act == "cold")
			progress('cold');
	}
	
	/**
	* Fonction retournant la température ambiante de la piece
	*/	
	function getTemperature() {
		return $("#cache").width();
	}
	
	/**
	* Fonction qui change la température ambiante de la piece
	*/	
	function setTemperature(v) {
		$("#cache").width(v);
	}
} );
